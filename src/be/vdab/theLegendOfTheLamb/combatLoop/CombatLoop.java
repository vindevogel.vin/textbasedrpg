package be.vdab.theLegendOfTheLamb.combatLoop;

import be.vdab.theLegendOfTheLamb.keyboardHelper.KeyboardHelper;
import be.vdab.theLegendOfTheLamb.unit.Player;
import be.vdab.theLegendOfTheLamb.unit.nPC.NPC;
import be.vdab.theLegendOfTheLamb.utility.parser.CommandParser;

import java.lang.reflect.InvocationTargetException;
import java.security.Key;

/**
 * @author Runtime Terrors
 * created on 6/07/2021
 */
public class CombatLoop {
    CommandParser cp;

    public static void combatLoop(Player player, NPC enemy) {
        while (player.getCurrentHitPoints() > 0 && enemy.getCurrentHitPoints() > 0) {
            System.out.println("What do you want to do?");
            System.out.println("1. Basic attack");
            System.out.println("2. Special attack");
            System.out.println("3. Check inventory");
            System.out.println("4. Attempt to flee");
            switch (KeyboardHelper.askForNumber(4)){
                case 1:
                    int damage = player.basicAttack();
                    enemy.setCurrentHitPoints(enemy.getCurrentHitPoints()-damage);
                    break;
                case 2:
                    break;
                case 3:
                    break;
                case 4:
                    break;
            }


        }
    }

}
