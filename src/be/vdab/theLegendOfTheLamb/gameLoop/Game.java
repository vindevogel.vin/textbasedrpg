package be.vdab.theLegendOfTheLamb.gameLoop;

import be.vdab.theLegendOfTheLamb.keyboardHelper.KeyboardHelper;
import be.vdab.theLegendOfTheLamb.map.GameMap;
import be.vdab.theLegendOfTheLamb.menus.MainMenu;
import be.vdab.theLegendOfTheLamb.unit.Player;
import be.vdab.theLegendOfTheLamb.map.Tile;
import be.vdab.theLegendOfTheLamb.utility.parser.CommandCollection;
import be.vdab.theLegendOfTheLamb.utility.parser.CommandParser;

import java.lang.reflect.InvocationTargetException;

/**
 * @author Runtime Terrors
 * created on 5/07/2021
 */

public class Game {
    private static Player player = null;
    private static GameMap gameMap = null;
    CommandParser cp;

    public void startGame() throws InvocationTargetException, IllegalAccessException {
        getPlayerAndMap();
        stillAlive();
    }

    private void getPlayerAndMap() {
        MainMenu mainMenu = new MainMenu();
        while (player == null || gameMap == null) {
            mainMenu.startMenu();
            if (mainMenu.getPlayer() != null) {
                player = mainMenu.getPlayer();
            }
            if (mainMenu.getMap() != null) {
                gameMap = mainMenu.getMap();
            }
        }
        cp = new CommandParser(player);
    }

    private void stillAlive() throws InvocationTargetException, IllegalAccessException {
        while (player.getCurrentHitPoints() > 0) {
            Tile currentTile = gameMap.getTiles()[player.getCurrentXPosition()][player.getCurrentYPosition()];
            System.out.println(currentTile.getDescription());
            KeyboardHelper.askForKeyword(cp);
        }
    }

}
