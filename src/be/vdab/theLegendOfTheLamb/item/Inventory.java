package be.vdab.theLegendOfTheLamb.item;

import be.vdab.theLegendOfTheLamb.item.carryable.Carryable;

import java.io.Serializable;
import java.util.*;

/**
 * @author Runtime Terrors
 * created on 6/07/2021
 */
public class Inventory implements Serializable {
    private List<Carryable> inventory = new LinkedList<>();
    private int inventorySize = 0;

    public Inventory() {
    }

    public List<Carryable> getInventory() {
        return inventory;
    }

    public void setInventory(List<Carryable> inventory) {
        this.inventory = inventory;
    }

    public void setInventorySize(int strength) {
        this.inventorySize = strength * 10;
    }

    public int getInventorySize() {
        return inventorySize;
    }

    @Override
    public String toString() {
        return "Inventory{" +
                "inventory=" + inventory +
                '}';
    }
}
