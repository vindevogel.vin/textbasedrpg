package be.vdab.theLegendOfTheLamb.item;

import java.io.Serializable;

/**
 * @author Runtime Terrors
 * created on 5/07/2021
 */
public abstract class Item implements Serializable {
    private String name;
    private String location;
    private String description;
    private String[] keywords;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String[] getKeywords() {
        return keywords;
    }

    public void setKeywords(String[] keywords) {
        this.keywords = keywords;
    }

    public abstract void use();

}
