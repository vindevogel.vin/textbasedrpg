package be.vdab.theLegendOfTheLamb.item.carryable;

import be.vdab.theLegendOfTheLamb.item.Item;

import java.io.Serializable;

/**
 * @author Runtime Terrors
 * created on 5/07/2021
 */
public interface Carryable extends Serializable {
    int itemSize = 0;
}
