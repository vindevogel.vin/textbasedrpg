package be.vdab.theLegendOfTheLamb.item.carryable.weapons;

import be.vdab.theLegendOfTheLamb.item.Item;

/**
 * @author Runtime Terrors
 * created on 7/07/2021
 */
public class Excalibur extends Weapon {

    public Excalibur() {
        this.setDamage(2);
        this.setFlavourDescription("You swing this beautiful sword for a clean hit dealing "+this.getDamage()+" damage");
    }

    @Override
    public void attack() {

    }

    @Override
    public void use() {

    }
}
