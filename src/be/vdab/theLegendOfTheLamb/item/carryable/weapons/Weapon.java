package be.vdab.theLegendOfTheLamb.item.carryable.weapons;

import be.vdab.theLegendOfTheLamb.item.Item;
import be.vdab.theLegendOfTheLamb.item.carryable.Carryable;

/**
 * @author Runtime Terrors
 * created on 6/07/2021
 */
public abstract class Weapon extends Item implements Carryable {
    private int damage;
    private String flavourDescription;

    public int getDamage() {
        return damage;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }

    public String getFlavourDescription() {
        return flavourDescription;
    }

    public void setFlavourDescription(String flavourDescription) {
        this.flavourDescription = flavourDescription;
    }

    public abstract void attack();
}
