package be.vdab.theLegendOfTheLamb.item.environment;

import be.vdab.theLegendOfTheLamb.unit.Player;

/**
 * @author Runtime Terrors
 * created on 8/07/2021
 */
public class Bed extends Environment {
    private final String description = "This looks like a cozy bed you can use to sleep in.";

    @Override
    public void interact(Player player) {
        player.setCurrentHitPoints(player.getMaxHitPoints());
        System.out.printf("%d/%d You feel revitalized.", player.getCurrentHitPoints(), player.getMaxHitPoints());
    }

    @Override
    public void use() {

    }
}
