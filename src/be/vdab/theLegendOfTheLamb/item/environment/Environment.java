package be.vdab.theLegendOfTheLamb.item.environment;

import be.vdab.theLegendOfTheLamb.item.Item;
import be.vdab.theLegendOfTheLamb.unit.Player;
import be.vdab.theLegendOfTheLamb.utility.Interactable;

/**
 * @author Runtime Terrors
 * created on 6/07/2021
 */
public abstract class Environment extends Item implements Interactable {

    public abstract void interact(Player player);

}
