package be.vdab.theLegendOfTheLamb.keyboardHelper;

import be.vdab.theLegendOfTheLamb.utility.parser.CommandParser;

import java.lang.reflect.InvocationTargetException;
import java.util.Scanner;

/**
 * @author Runtime Terrors
 * created on 5/07/2021
 */

public class KeyboardHelper {
    private static final Scanner KEYBOARD = new Scanner(System.in);

    public static void askForKeyword(CommandParser commandParser) throws InvocationTargetException, IllegalAccessException {
        System.out.print("> ");
        commandParser.parse(KEYBOARD.nextLine());
    }

    public static int askForNumber(int i) {
        System.out.print("> ");
        int number = KEYBOARD.nextInt();
        while(number > i || number <= 0) {
            number = KEYBOARD.nextInt();
        }
        return number;
    }

    public static String askForName() {
        System.out.print("> ");
        String name = KEYBOARD.next();
        return name;
    }
}
