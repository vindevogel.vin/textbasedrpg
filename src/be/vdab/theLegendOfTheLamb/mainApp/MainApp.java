package be.vdab.theLegendOfTheLamb.mainApp;

import be.vdab.theLegendOfTheLamb.gameLoop.Game;
import be.vdab.theLegendOfTheLamb.unit.Player;
import be.vdab.theLegendOfTheLamb.utility.parser.CommandParser;

import java.lang.reflect.InvocationTargetException;
import java.util.Scanner;

/**
 * @author Runtime Terrors
 * created on 5/07/2021
 */
public class MainApp {

    public static void main(String[] args) throws InvocationTargetException, IllegalAccessException {

//        CommandParser cp = new CommandParser(new Player());
//        cp.test();
        Game game = new Game();
        game.startGame();
    }
}
