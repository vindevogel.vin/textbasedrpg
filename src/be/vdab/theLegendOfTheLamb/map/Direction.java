package be.vdab.theLegendOfTheLamb.map;

/**
 * @author Runtime Terrors
 * created on 5/07/2021
 */
public enum Direction {
    NORTH(-1, 0),
    SOUTH(1, 0),
    WEST(0, -1),
    EAST(0, 1);

    int xMovement;
    int yMovement;

    Direction(int xMovement, int yMovement) {
        this.xMovement = xMovement;
        this.yMovement = yMovement;
    }

    public int getxMovement() {
        return xMovement;
    }

    public void setxMovement(int xMovement) {
        this.xMovement = xMovement;
    }

    public int getyMovement() {
        return yMovement;
    }

    public void setyMovement(int yMovement) {
        this.yMovement = yMovement;
    }

    @Override
    public String toString() {
        return "Direction{" +
                "xMovement=" + xMovement +
                ", yMovement=" + yMovement +
                '}';
    }
}
