package be.vdab.theLegendOfTheLamb.map;

import java.io.Serializable;

/**
 * @author Runtime Terrors
 * created on 5/07/2021
 */
public class GameMap implements Serializable {
    private Tile[][] tiles;

    public GameMap(Tile[][] tiles) {
        this.tiles = tiles;
    }

    public void setTiles(Tile[][] tiles) {
        this.tiles = tiles;
    }

    public Tile[][] getTiles() {
        return tiles;
    }
}
