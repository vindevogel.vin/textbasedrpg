package be.vdab.theLegendOfTheLamb.map;

import be.vdab.theLegendOfTheLamb.item.carryable.weapons.Excalibur;
import be.vdab.theLegendOfTheLamb.unit.nPC.enemy.Orc;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Runtime Terrors
 * created on 6/07/2021
 */
public class MapLegend {
    public static Tile generateTile (char tileType, int xpos, int ypos){
        TileType typeOfTile = null;
        for (TileType type : TileType.values()) {
            if(type.getTranslation() == tileType){
                typeOfTile = type;
                break;
            }
        }
        Tile tile = new Tile(xpos, ypos, typeOfTile);

        try {
            switch (typeOfTile) {
                case WATER:
                    tile.setDescription("You are swimming in a body of water");
                    tile.setDescriptionWhenLooking("A body of water");
                    tile.setWalkable(typeOfTile.isWalkable());
                    break;
                case BRIDGE:
                    tile.setDescription("You are on a bridge.");
                    tile.setDescriptionWhenLooking("You see a bridge, you can cross that.");
                    tile.setWalkable(typeOfTile.isWalkable());
                    tile.addNPC(new Orc());
                    break;
                case PATH:
                    tile.setDescription("You are on a path");
                    tile.setWalkable(typeOfTile.isWalkable());
                    break;
                case FOREST:
                    tile.setDescription("While the burning of the bush was a fantastic idea, a whole forest might be a bit much");
                    tile.setWalkable(typeOfTile.isWalkable());
                    break;
                case BUSH:
                    tile.setDescription("This bush looks flamable");
                    tile.setWalkable(typeOfTile.isWalkable());
                    break;
                case ROAD:
                    tile.setDescription("Yes, you can walk here...");
                    tile.setWalkable(typeOfTile.isWalkable());
                    break;
                case HALLWAY:
                    tile.setDescription("You are in a hallway.");
                    tile.setWalkable(typeOfTile.isWalkable());
                    break;
                case WALL:
                    tile.setDescription("That is a wall, we've been over this");
                    tile.setWalkable(typeOfTile.isWalkable());
                    break;
                case LEGENDARYITEMSPOT:
                    tile.setDescription("You see something shiny, it might be worth checking it out");
                    tile.setWalkable(typeOfTile.isWalkable());
                    break;
                case BOSSTILE:
                    tile.setDescription("That looks like Sven");
                    tile.setWalkable(typeOfTile.isWalkable());
                    break;
                case FIELD:
                    tile.setDescription("You see an open area");
                    tile.setWalkable(typeOfTile.isWalkable());
                    break;
                case DUNGEONROOM:
                    tile.setDescription("You see a room");
                    tile.setWalkable(typeOfTile.isWalkable());
                    break;
                case DUNGEONPATH:
                    tile.setDescription("You see a path you can take");
                    tile.setWalkable(typeOfTile.isWalkable());
                    break;
                case SMALLENCOUNTER:
                    tile.setDescription("You see a small creature");
                    tile.setWalkable(typeOfTile.isWalkable());
                    break;
                case SHOPKEEPER:
                    tile.setDescription("That looks like a shop");
                    tile.setWalkable(typeOfTile.isWalkable());
                    break;
                case MINIBOSS:
                    tile.setDescription("You see a vague silhouette");
                    tile.setWalkable(typeOfTile.isWalkable());
                    tile.addNPC(new Orc());
                    break;
                case DOOR:
                    tile.setDescription("This is a door, if you don't have the key you can't enter through here");
                    tile.setWalkable(typeOfTile.isWalkable());
                    break;
                case ROOM:
                    tile.setDescription("You see a room but aren't sure what's inside of it");
                    tile.setWalkable(typeOfTile.isWalkable());
                    break;
                case ITEMCHEST:
                    tile.setDescription("You think you see a chest shining in the distance, maybe you can go check if it's a real one");
                    tile.setWalkable(typeOfTile.isWalkable());
                    tile.addItem(new Excalibur());
                    break;
                case MANSIONENTRANCE:
                    tile.setDescription("You think you see a chest shining in the distance, maybe you can go check if it's a real one");
                    tile.setWalkable(typeOfTile.isWalkable());
                    break;
                case SECRETPASSAGE:
                    tile.setDescription("You think you see a chest shining in the distance, maybe you can go check if it's a real one");
                    tile.setWalkable(typeOfTile.isWalkable());
                    break;
                case SECRETBUTTONTILE:
                    tile.setDescription("You think you see a chest shining in the distance, maybe you can go check if it's a real one");
                    tile.setWalkable(typeOfTile.isWalkable());
                    break;
                case KEYTILE:
                    tile.setDescription("You think you see a chest shining in the distance, maybe you can go check if it's a real one");
                    tile.setWalkable(typeOfTile.isWalkable());
                    break;
            }
        }catch (NullPointerException ne){
            System.err.println(tileType);
            System.exit(-1);
        }
        return tile;
    }
}
