package be.vdab.theLegendOfTheLamb.map;

import be.vdab.theLegendOfTheLamb.item.Item;
import be.vdab.theLegendOfTheLamb.unit.Unit;
import be.vdab.theLegendOfTheLamb.unit.nPC.NPC;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Runtime Terrors
 * created on 5/07/2021
 */
public class Tile implements Serializable {
    private int positionX;
    private int positionY;
    private TileType tileType;
    private List<Item> items = new LinkedList<>();
    private List<NPC> nPCs = new LinkedList<>();
    private String description;
    private String descriptionWhenLooking;
    private boolean walkable;

    public Tile(int posx, int posy, TileType tileType){
        this.positionX = posx;
        this.positionY = posy;
        this.tileType = tileType;
        this.walkable = tileType.isWalkable();

    }

    public String getDescriptionWhenLooking() {
        return descriptionWhenLooking;
    }

    public void setDescriptionWhenLooking(String descriptionWhenLooking) {
        this.descriptionWhenLooking = descriptionWhenLooking;
    }

    public void setWalkable(boolean walkable) {
        this.walkable = walkable;
    }

    public boolean isWalkable() {
        return walkable;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public int getPositionX() {
        return positionX;
    }

    public void setPositionX(int positionX) {
        this.positionX = positionX;
    }

    public int getPositionY() {
        return positionY;
    }

    public void setPositionY(int positionY) {
        this.positionY = positionY;
    }

    public TileType getTileType() {
        return tileType;
    }

    public void setTileType(TileType tileType) {
        this.tileType = tileType;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public List<NPC> getNPCs() {
        return nPCs;
    }

    public void setNPCs(List<NPC> nPCs) {
        this.nPCs = nPCs;
    }
    public void addNPC(NPC npc){
        nPCs.add(npc);
    }
    public void addItem(Item item){
        items.add(item);
    }

    @Override
    public String toString() {
        return "Tile{" +
                "positionX=" + positionX +
                ", positionY=" + positionY +
                ", tileType=" + tileType +
                ", items=" + items +
                ", nPCs=" + nPCs +
                '}';
    }
}
