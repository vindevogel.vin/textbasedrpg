package be.vdab.theLegendOfTheLamb.map;

import be.vdab.theLegendOfTheLamb.item.Item;
import be.vdab.theLegendOfTheLamb.item.carryable.weapons.Excalibur;
import be.vdab.theLegendOfTheLamb.unit.nPC.NPC;
import be.vdab.theLegendOfTheLamb.unit.nPC.enemy.Orc;

import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Runtime Terrors
 * created on 6/07/2021
 */
public enum TileType {
    FOREST(false, '#'),
    ROAD(true, 'r'),
    PATH(true, '_'),
    HALLWAY(true, 'h'),
    WATER(false, '~'),
    WALL(false, 'W'),
    BRIDGE(true, '^'),
    BUSH(false, '*'),
    LEGENDARYITEMSPOT(false, 'L'),
    BOSSTILE(true, 'B'),
    FIELD(true, 'F'),
    DUNGEONROOM(true, 'd'),
    DUNGEONPATH(true, 'p'),
    SMALLENCOUNTER(true, 's'),
    SHOPKEEPER(true, '$'),
    MINIBOSS(true, 'M'),
    DOOR(false, 'D'),
    ROOM(true, 'R'),
    ITEMCHEST(true, 'I'),
    MANSIONENTRANCE(true, 'E'),
    KEYTILE(true, 'K'),
    SECRETPASSAGE(false, 'S'),
    SECRETBUTTONTILE(true, 'b');
    boolean walkable;
    char translation;

    public boolean isWalkable() {
        return walkable;
    }

    public char getTranslation() {
        return translation;
    }

    TileType(boolean walkable, char translation){
        this.walkable = walkable;
        this.translation = translation;
    }
}
