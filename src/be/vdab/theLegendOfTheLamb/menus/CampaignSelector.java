package be.vdab.theLegendOfTheLamb.menus;

import be.vdab.theLegendOfTheLamb.keyboardHelper.KeyboardHelper;
import be.vdab.theLegendOfTheLamb.map.GameMap;
import be.vdab.theLegendOfTheLamb.map.MapLegend;
import be.vdab.theLegendOfTheLamb.map.Tile;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * @author Runtime Terrors
 * created on 5/07/2021
 */
public class CampaignSelector {
    private static final Tile[][] MAP = new Tile[32][32];
    private static final GameMap FINAL_GAME_MAP = new GameMap(MAP);
    private static final Path HAUNTED_MANSION = Path.of(".\\src\\be\\vdab\\theLegendOfTheLamb\\map\\originalMaps\\HauntedMansion.txt");
    private static final Path RIVER_OF_TEARS = Path.of(".\\src\\be\\vdab\\theLegendOfTheLamb\\map\\originalMaps\\TheRiverOfTears.txt");
    private static final Path DRAGON_MOUNTAIN = Path.of(".\\src\\be\\vdab\\theLegendOfTheLamb\\map\\originalMaps\\DragonMountain.txt");

    public static GameMap selectCampaign(){
        System.out.println("1. Haunted Mansion");
        System.out.println("2. The River of Tears");
        System.out.println("3. Dragon Mountain");
        System.out.println("Please make your choice: ");
        switch(KeyboardHelper.askForNumber(3)) {
            case 1:

                generateMap(HAUNTED_MANSION);
//                System.out.println(MAP[3][3].toString());
                break;
            case 2:
                generateMap(RIVER_OF_TEARS);
                break;
            case 3:
                generateMap(DRAGON_MOUNTAIN);
                break;
            default:
                break;
        }
        return FINAL_GAME_MAP;
    }
    private static void generateMap(Path chosenMap){
        try(BufferedReader reader = Files.newBufferedReader(chosenMap)){
            int countx = 0;
            int county = 0;
            String line;
            while((line = reader.readLine()) != null){
                for (char c:line.toCharArray()) {
                    MAP[countx][county] = MapLegend.generateTile(c, countx, county);
                    county++;
                }
                county = 0;
                countx++;
            }
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
        FINAL_GAME_MAP.setTiles(MAP);
    }
}
