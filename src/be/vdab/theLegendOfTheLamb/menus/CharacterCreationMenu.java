package be.vdab.theLegendOfTheLamb.menus;

import be.vdab.theLegendOfTheLamb.keyboardHelper.KeyboardHelper;
import be.vdab.theLegendOfTheLamb.map.GameMap;
import be.vdab.theLegendOfTheLamb.unit.Player;
import be.vdab.theLegendOfTheLamb.unit.races.Races;
import be.vdab.theLegendOfTheLamb.unit.unitclass.UnitClass;

/**
 * @author Runtime Terrors
 * created on 5/07/2021
 */
public class CharacterCreationMenu {
    private static Player player = new Player();

    public static Player creation(GameMap map) {
        playerName();
        playerGender();
        player.setCurrentMap(map);
        playerRace();
        playerClass();
        playerAttributes();
        playerStats();
        player.inventorySize();
        startPosition();
        player.returnValues();

        return player;
    }

    public static void startPosition() {
        player.setCurrentXPosition(30);
        player.setCurrentYPosition(15);
    }

    private static void playerName() {
        System.out.println("What is the name of your adventurer?");
        player.setName(KeyboardHelper.askForName());
    }

    private static void playerGender() {
        System.out.println("Is " + player.getName() + " \n1: male\n2: female");
        switch (KeyboardHelper.askForNumber(2)) {
            case 1:
                player.setMale(true);
                break;
            case 2:
                player.setMale(false);
                break;
            default:
                break;
        }
    }

    private static void playerRace() {
        Races[] arrayOfRaces = Races.values();
        System.out.println("Is " + player.getName() + " a:");
        for (int i = 0; i < arrayOfRaces.length; i++){
            System.out.println((i+1) + ": " + arrayOfRaces[i]);
        }
        player.setRace(arrayOfRaces[KeyboardHelper.askForNumber(arrayOfRaces.length)-1]);
    }

    private static void playerClass() {
        UnitClass[] arrayOfClasses = UnitClass.values();
        System.out.println("Is " + player.getName() + " a");
        for (int i = 0; i < arrayOfClasses.length; i++){
            System.out.println((i+1) + ": " + arrayOfClasses[i]);
        }
        player.setUnitClass(arrayOfClasses[KeyboardHelper.askForNumber(arrayOfClasses.length)-1]);
    }

    private static void playerAttributes() {
        Races race = player.getRace();
        UnitClass unitClass = player.getUnitClass();
        player.setStrength(10 + race.getStrength() + unitClass.getStrength());
        player.setConstitution(10 + race.getConstitution() + unitClass.getConstitution());
        player.setCharisma(10 + race.getCharisma() + unitClass.getCharisma());
        player.setDexterity(10 + race.getDexterity() + unitClass.getDexterity());
        player.setIntelligence(10 + race.getIntelligence() + unitClass.getIntelligence());
        player.setWisdom(10 + race.getWisdom() + unitClass.getWisdom());
        player.setSpeed(race.getSpeed());
    }

    private static void playerStats() {
        player.setMaxHitPoints(player.getConstitution() * 5);
        player.setManaPoints(player.getWisdom() * 5);
        player.setStaminaPoints(player.getDexterity() * 5);
        player.setInitiative(5);
        player.setCurrentHitPoints(player.getMaxHitPoints());
    }
}
