package be.vdab.theLegendOfTheLamb.menus;

import be.vdab.theLegendOfTheLamb.utility.parser.Command;
import be.vdab.theLegendOfTheLamb.utility.parser.CommandCollection;

import java.lang.reflect.Method;

/**
 * @author Runtime Terrors
 * created on 6/07/2021
 */
public class Controls {

    public static void displayControls() {
        Method[] methods = CommandCollection.class.getMethods();

        for(Method method: methods){
            if (!method.isAnnotationPresent(Command.class)) {
                continue;
            }
            Command annotation = method.getAnnotation(Command.class);
            System.out.print(annotation.command() + ": ");
            for(String alias : annotation.aliases()){
                if (alias.length() != 0) {
                    System.out.print(alias + " ");
                }
            }
            System.out.println();
        }

    }
}
