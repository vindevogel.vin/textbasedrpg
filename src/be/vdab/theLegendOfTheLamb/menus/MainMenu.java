package be.vdab.theLegendOfTheLamb.menus;

import be.vdab.theLegendOfTheLamb.keyboardHelper.KeyboardHelper;
import be.vdab.theLegendOfTheLamb.map.GameMap;
import be.vdab.theLegendOfTheLamb.unit.Player;

import java.io.*;
import java.nio.file.Path;


/**
 * @author Runtime Terrors
 * created on 5/07/2021
 */
public class MainMenu {
    private Player player;
    private GameMap gameMap;

    public void startMenu() {
        System.out.println("1. New Game");
        System.out.println("2. Load Game");
        System.out.println("3. Controls");
        System.out.println("4. Quit Game");

        switch(KeyboardHelper.askForNumber(4)) {
            case 1:
                NewGame newGame = new NewGame();
                newGame.startNewGame();
                player = newGame.getPlayer();
                gameMap = newGame.getMap();
                return;
            case 2:
                loadGame();
                return;
            case 3:
                Controls.displayControls();
                return;
            case 4:
                System.exit(0);
        }
    }



    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public GameMap getMap() {
        return gameMap;
    }

    public void setMap(GameMap gameMap) {
        this.gameMap = gameMap;
    }
    public void loadGame(){
//        Path saveDirectory = Path.of(".\\src\\be\\vdab\\theLegendOfTheLamb\\saves\\");

        try(FileInputStream saveFile = new FileInputStream(".\\src\\be\\vdab\\theLegendOfTheLamb\\saves\\SaveFile.save");
            ObjectInputStream in = new ObjectInputStream(saveFile)){
            System.out.println("loading stuff");
            player = (Player) in.readObject();
            System.out.println(player.getRace());
            System.out.println(player.getName());
            System.out.println(player.getUnitClass());
            gameMap = player.getCurrentMap();
        } catch (ClassNotFoundException | IOException e) {
            e.printStackTrace();
        }
    }
}
