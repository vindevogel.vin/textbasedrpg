package be.vdab.theLegendOfTheLamb.menus;

import be.vdab.theLegendOfTheLamb.map.GameMap;
import be.vdab.theLegendOfTheLamb.unit.Player;

/**
 * @author Runtime Terrors
 * created on 6/07/2021
 */
public class NewGame {
    private Player player;
    private GameMap gameMap;
    private CampaignSelector campaignSelector;
    public NewGame() {
    }

    public void startNewGame(){
        System.out.println("You want to start a new game, what campaign would you like to play?");
        gameMap = CampaignSelector.selectCampaign();
        player = CharacterCreationMenu.creation(gameMap);
    }

    public Player getPlayer() {
        return player;
    }

    public GameMap getMap() {
        return gameMap;
    }
}
