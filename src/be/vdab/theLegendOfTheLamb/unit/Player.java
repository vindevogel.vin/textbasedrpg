package be.vdab.theLegendOfTheLamb.unit;

import be.vdab.theLegendOfTheLamb.combatLoop.CombatLoop;
import be.vdab.theLegendOfTheLamb.item.Inventory;
import be.vdab.theLegendOfTheLamb.item.carryable.armour.Armour;
import be.vdab.theLegendOfTheLamb.item.carryable.weapons.Excalibur;
import be.vdab.theLegendOfTheLamb.item.carryable.weapons.Weapon;
import be.vdab.theLegendOfTheLamb.map.Direction;
import be.vdab.theLegendOfTheLamb.map.GameMap;
import be.vdab.theLegendOfTheLamb.map.Tile;
import be.vdab.theLegendOfTheLamb.unit.nPC.NPC;
import be.vdab.theLegendOfTheLamb.unit.races.Races;
import be.vdab.theLegendOfTheLamb.unit.unitclass.UnitClass;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Path;

/**
 * @author Runtime Terrors
 * created on 5/07/2021
 */
public class Player extends Unit {

    private int experience;
    private Races race;
    private UnitClass unitClass;
    private final Inventory inventory = new Inventory();
    private Weapon equippedWeapon = new Excalibur();
    private Armour equippedArmor;
    private int currentXPosition;
    private int currentYPosition;
    public static final long serialVersionUID = -4467076312537915352L;
    private GameMap currentMap;

    public Races getRace() {
        return race;
    }


    public void setRace(Races race) {
        this.race = race;
    }

    public UnitClass getUnitClass() {
        return unitClass;
    }

    public void setUnitClass(UnitClass unitClass) {
        this.unitClass = unitClass;
    }

    public void inventorySize() {
        this.inventory.setInventorySize(this.getStrength());
    }

    public GameMap getCurrentMap() {
        return currentMap;
    }

    public void setCurrentMap(GameMap currentMap) {
        this.currentMap = currentMap;
    }

    public int basicAttack() {
        System.out.println(equippedWeapon.getFlavourDescription());
        return equippedWeapon.getDamage();
    }

    public int getExperience() {
        return experience;
    }

    public void setExperience(int experience) {
        this.experience = experience;
    }

    public Inventory getInventory() {
        return inventory;
    }

    public Weapon getEquippedWeapon() {
        return equippedWeapon;
    }

    public void setEquippedWeapon(Weapon equippedWeapon) {
        this.equippedWeapon = equippedWeapon;
    }

    public Armour getEquippedArmor() {
        return equippedArmor;
    }

    public void setEquippedArmor(Armour equippedArmor) {
        this.equippedArmor = equippedArmor;
    }

    public int getCurrentXPosition() {
        return currentXPosition;
    }

    public void setCurrentXPosition(int currentXPosition) {
        this.currentXPosition = currentXPosition;
    }

    public int getCurrentYPosition() {
        return currentYPosition;
    }

    public void setCurrentYPosition(int currentYPosition) {
        this.currentYPosition = currentYPosition;
    }

    public void move(String userCommand) {
        userCommand = userCommand.toUpperCase();
        try {
            Direction direction = Direction.valueOf(userCommand);
            Tile testTile = currentMap.getTiles()[currentXPosition + direction.getxMovement()][currentYPosition + direction.getyMovement()];
            System.out.println(testTile.toString());
            System.out.println(testTile.isWalkable());
            if (currentMap.getTiles()[currentXPosition + direction.getxMovement()][currentYPosition + direction.getyMovement()].isWalkable()) {
                setCurrentYPosition(currentYPosition + direction.getyMovement());
                setCurrentXPosition(currentXPosition + direction.getxMovement());
            } else {
                System.out.println("You can't go this way");
            }
        } catch (IllegalArgumentException iae) {
            System.err.println("No such direction");
        }
    }

    public static void consumeItem(String args) {
        System.out.println(args);
        //TODO 1 item uit inventory consumeren die overeenkomt met args.
    }

    public static void attackNPC() {

    }

    public static void saveGame(Player player/*, String args*/) {
        Path pathToSaveFile = Path.of(".\\src\\be\\vdab\\theLegendOfTheLamb\\saves\\SaveFile.save");
        try (FileOutputStream saveFile = new FileOutputStream(String.valueOf(pathToSaveFile));
             ObjectOutputStream out = new ObjectOutputStream(saveFile);) {
            out.writeObject(player);
            System.out.println("The game is saved!");
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }

    public void startCombat() throws InvocationTargetException, IllegalAccessException {
        if (currentMap.getTiles()[currentXPosition][currentYPosition].getNPCs().get(0) != null) {
            NPC enemy = currentMap.getTiles()[currentXPosition][currentYPosition].getNPCs().get(0);
            CombatLoop.combatLoop(this, enemy);
            System.out.println(enemy.getCurrentHitPoints());
            System.out.println("this too");
        }
    }
}
