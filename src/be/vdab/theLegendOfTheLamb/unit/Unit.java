package be.vdab.theLegendOfTheLamb.unit;

import java.io.Serializable;

/**
 * @author Runtime Terrors
 * created on 5/07/2021
 */
public class Unit implements Serializable {
    private int strength;
    private int intelligence;
    private int wisdom;
    private int dexterity;
    private int constitution;
    private int charisma;
    private String name;
    private boolean isMale;
    private int maxHitPoints;
    private int currentHitPoints;
    private int manaPoints;
    private int staminaPoints;
    private int speed;
    private int armourClass;
    private int initiative;

    public Unit() {
    }

    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public int getIntelligence() {
        return intelligence;
    }

    public int getMaxHitPoints() {
        return maxHitPoints;
    }

    public void setMaxHitPoints(int maxHitPoints) {
        this.maxHitPoints = maxHitPoints;
    }

    public int getCurrentHitPoints() {
        return currentHitPoints;
    }

    public void setCurrentHitPoints(int currentHitPoints) {
        this.currentHitPoints = currentHitPoints;
    }

    public void setIntelligence(int intelligence) {
        this.intelligence = intelligence;
    }

    public int getWisdom() {
        return wisdom;
    }

    public void setWisdom(int wisdom) {
        this.wisdom = wisdom;
    }

    public int getDexterity() {
        return dexterity;
    }

    public void setDexterity(int dexterity) {
        this.dexterity = dexterity;
    }

    public int getConstitution() {
        return constitution;
    }

    public void setConstitution(int constitution) {
        this.constitution = constitution;
    }

    public int getCharisma() {
        return charisma;
    }

    public void setCharisma(int charisma) {
        this.charisma = charisma;
    }

    public boolean isMale() {
        return isMale;
    }

    public void setMale(boolean male) {
        isMale = male;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getManaPoints() {
        return manaPoints;
    }

    public void setManaPoints(int manaPoints) {
        this.manaPoints = manaPoints;
    }

    public int getStaminaPoints() {
        return staminaPoints;
    }

    public void setStaminaPoints(int staminaPoints) {
        this.staminaPoints = staminaPoints;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public int getArmourClass() {
        return armourClass;
    }

    public void setArmourClass(int armourClass) {
        this.armourClass = armourClass;
    }

    public int getInitiative() {
        return initiative;
    }

    public void setInitiative(int initiative) {
        this.initiative = initiative;
    }

    public void returnValues(){
        System.out.println(charisma);
        System.out.println(intelligence);
        System.out.println(speed);
        System.out.println(strength);
        System.out.println(staminaPoints);
        System.out.println(manaPoints);
        System.out.println(maxHitPoints);
        System.out.println(currentHitPoints);
        System.out.println(wisdom);
        System.out.println(dexterity);
        System.out.println(initiative);
        System.out.println(constitution);
    }

    public void takeDamage(int damage){
        currentHitPoints -= damage;
    }
    public void healDamage(int heal){
        currentHitPoints += heal;
        if (currentHitPoints > manaPoints){
            currentHitPoints = maxHitPoints;
        }
    }

    //TODO add methods for all units

//    public int basicAttack(){
//        System.out.println("description of attack");
//        return 15;
//    }
}
