package be.vdab.theLegendOfTheLamb.unit.nPC.enemy;

import be.vdab.theLegendOfTheLamb.unit.nPC.NPC;

/**
 * @author Runtime Terrors
 * created on 6/07/2021
 */
public class Orc extends NPC {

    public Orc() {
        this.setMaxHitPoints(10);
        this.setCurrentHitPoints(10);
    }
}
