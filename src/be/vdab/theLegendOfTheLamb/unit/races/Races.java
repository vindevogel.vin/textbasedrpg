package be.vdab.theLegendOfTheLamb.unit.races;

public enum Races {
    HUMAN(1,1,1,1,1,1,6),
    ELF(0,0,3,0,3,0,7),
    DWARF(0,3,0,3,0,0,5);

    int wisdom;
    int constitution;
    int intelligence;
    int strength;
    int dexterity;
    int charisma;
    int speed;

    public int getWisdom() {
        return wisdom;
    }

    public int getConstitution() {
        return constitution;
    }

    public int getIntelligence() {
        return intelligence;
    }

    public int getStrength() {
        return strength;
    }

    public int getDexterity() {
        return dexterity;
    }

    public int getCharisma() {
        return charisma;
    }

    public int getSpeed() {
        return speed;
    }

    Races(int wisdom, int constitution, int intelligence, int strength, int dexterity, int charisma, int speed){
        this.wisdom = wisdom;
        this.constitution = constitution;
        this.intelligence = intelligence;
        this.strength = strength;
        this.dexterity = dexterity;
        this.charisma = charisma;
        this.speed = speed;
    }
}
