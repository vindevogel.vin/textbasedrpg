package be.vdab.theLegendOfTheLamb.unit.unitclass;

/**
 * @author Runtime Terrors
 * created on 6/07/2021
 */
public enum UnitClass {
    MAGE(3,0,2,0,0,0),
    WARRIOR(0,2,0,3,0,0),
    MONK(7,0,0,-3,0,0),
    RANGER(1,0,0,0,3,0),
    NERD(0,0,10,-5,0,0),
    ROGUE(0,0,1,0,3,0);

    int wisdom;
    int constitution;
    int intelligence;
    int strength;
    int dexterity;
    int charisma;

    public int getWisdom() {
        return wisdom;
    }

    public int getConstitution() {
        return constitution;
    }

    public int getIntelligence() {
        return intelligence;
    }

    public int getStrength() {
        return strength;
    }

    public int getDexterity() {
        return dexterity;
    }

    public int getCharisma() {
        return charisma;
    }


    UnitClass(int wisdom, int constitution, int intelligence, int strength, int dexterity, int charisma){
        this.wisdom = wisdom;
        this.constitution = constitution;
        this.intelligence = intelligence;
        this.strength = strength;
        this.dexterity = dexterity;
        this.charisma = charisma;
    }
}
