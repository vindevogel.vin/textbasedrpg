package be.vdab.theLegendOfTheLamb.utility;

import be.vdab.theLegendOfTheLamb.unit.Player;

/**
 * @author Runtime Terrors
 * created on 5/07/2021
 */
public interface Interactable {
    // aanvaardt keyword

    void interact(Player player);
}
