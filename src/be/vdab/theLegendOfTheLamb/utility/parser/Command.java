package be.vdab.theLegendOfTheLamb.utility.parser;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * @author Runtime Terrors
 * created on 8/07/2021
 */

@Retention(RetentionPolicy.RUNTIME)

public @interface Command {
        String command();
        String[] aliases();
        String description();
}

