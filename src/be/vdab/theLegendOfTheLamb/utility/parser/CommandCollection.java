package be.vdab.theLegendOfTheLamb.utility.parser;

import be.vdab.theLegendOfTheLamb.unit.Player;

import java.lang.reflect.InvocationTargetException;

/**
 * @author Runtime Terrors
 * created on 8/07/2021
 */
public class CommandCollection {

    @Command(command = "consume", aliases = {"drink", "eat", "swallow"}, description = "consumes item")
    public void commandConsume(Player player, String args){
        player.consumeItem(args);
    }
    @Command(command = "look", aliases = {"view", "examine"}, description = "looks in a direction")
    public void test2(Player player, String args){
        player.consumeItem(args);
    }
    @Command(command = "move", aliases = {"go", "goto", "walk"}, description = "relocates the player")
    public void commandMove(Player player, String args){
        player.move(args);
    }
    @Command(command = "equip", aliases = {"wear"}, description = "equips weapon/armour")
    public void test4(Player player, String args){
        player.consumeItem(args);
    }
    @Command(command = "pickup", aliases = {"grab", "take"}, description = "pick up item")
    public void test5(Player player, String args){
        player.consumeItem(args);
    }
    @Command(command = "drop", aliases = {"release"}, description = "drops item")
    public void test6(Player player, String args){
        player.consumeItem(args);
    }
    @Command(command = "attack", aliases = {"fight", "hit", "kill"}, description = "attack creature")
    public void commandAttack(Player player, String args) throws InvocationTargetException, IllegalAccessException {
        player.startCombat();
    }
    @Command(command = "flee", aliases = {"run", "escape"}, description = "tries to flee from combat")
    public void test8(Player player, String args){
        player.consumeItem(args);
    }
    @Command(command = "special", aliases = {"skill"}, description = "use special ability")
    public void test9(Player player, String args){
        player.consumeItem(args);
    }
    @Command(command = "use", aliases = {}, description = "use item")
    public void test10(Player player, String args){
        player.consumeItem(args);
    }
    @Command(command = "save", aliases = {}, description = "saves the game")
    public void commandSave(Player player, String args){
//        if (args.isEmpty()){
//            System.out.println("please give your save file a name");
//        }
//        else {
            player.saveGame(player);
//        }
    }

}
