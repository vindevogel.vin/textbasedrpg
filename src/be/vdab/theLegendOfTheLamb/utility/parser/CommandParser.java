package be.vdab.theLegendOfTheLamb.utility.parser;

import be.vdab.theLegendOfTheLamb.unit.Player;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.TreeMap;

/**
 * @author Runtime Terrors
 * created on 8/07/2021
 */
public class CommandParser {
    private TreeMap<String, Method> commandMap = new TreeMap<>();
    private Player player;

    public CommandParser(Player player) {
        initCommandMap();
        this.player = player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    private void initCommandMap() {
        Method[] methods = CommandCollection.class.getMethods();

        for(Method method: methods){
            if (!method.isAnnotationPresent(Command.class)) {
                continue;
            }
            Command annotation = method.getAnnotation(Command.class);
            this.commandMap.put(annotation.command(), method);
            for(String alias : annotation.aliases()){
                if (alias.length() == 0) {
                    break;
                }
                this.commandMap.put(alias, method);
            }
        }
    }

    public void parse(String usercommand) throws InvocationTargetException, IllegalAccessException {
        usercommand = removeNaturalText(usercommand);
        for (String key : commandMap.descendingKeySet()){
            if (usercommand.startsWith(key)){
                Method methodToUse = commandMap.get(key);
                methodToUse.invoke(new CommandCollection(),player, usercommand.substring(key.length()).trim());
            }
        }
    }

    private String removeNaturalText(String command) {
        command = command.replaceAll(" to ", " ");
        command = command.replaceAll(" a ", " ");
        return command;
    }

}
